export default {
    'support': 101,

    'aside-left-top': 102,
    'aside-left-middle': 103,
    'aside-left-bottom': 104,

    'aside-right-top': 105,
    'aside-right-middle': 106,
    'aside-right-bottom': 107,

    'main-0': 108,
    'main-1': 109,
    'main-2': 110,
    'main-3': 111,
    'main-4': 112,
    'main-5': 113,
    'main-6': 114,

    'footer': 115,

    'small-homepage': 116,
};
